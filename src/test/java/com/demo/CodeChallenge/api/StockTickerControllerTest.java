package com.demo.CodeChallenge.api;

import java.util.LinkedList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.demo.CodeChallenge.model.StockTicker;
import com.demo.CodeChallenge.service.StockTickerService;

/**
 * This class tests the StockTickerController methods using Mockito.
 */
@ExtendWith(MockitoExtension.class)
public class StockTickerControllerTest {

    private long id;
    private List<StockTicker> stockTickers;
    String stockTickerSymbol;

    @Mock
    private StockTicker aStockTicker;

    @Mock
    private StockTicker anotherStockTicker;

    @Mock
    private StockTickerService stockTickerService;

    @InjectMocks
    private StockTickerController stockTickerController;

    @Test
    public void testAdd() {
        stockTickerController.addStockTicker(aStockTicker);
        verify(stockTickerService).add(aStockTicker);
    }

    @Test
    public void testGetAll() {
        stockTickers = new LinkedList();
        stockTickers.add(aStockTicker);
        stockTickers.add(anotherStockTicker);
        when(stockTickerService.getAll()).thenReturn(stockTickers);

        List<StockTicker> results = stockTickerController.all();
        Assertions.assertEquals(2, results.size());
        Assertions.assertTrue(results.contains(aStockTicker));
        Assertions.assertTrue(results.contains(anotherStockTicker));

        verify(stockTickerService).getAll();
    }

    @Test
    public void testDelete() {
        stockTickerController.deleteById(id);
        verify(stockTickerService).delete(id);
    }

    @Test
    public void testGetByStockSymbol() {
        stockTickers = new LinkedList();
        stockTickers.add(aStockTicker);
        stockTickers.add(anotherStockTicker);
        when(stockTickerService.getByStockSymbol(stockTickerSymbol)).thenReturn(stockTickers);

        List<StockTicker> results = stockTickerController.getByStockSymbol(stockTickerSymbol);

        Assertions.assertEquals(2, results.size());
        Assertions.assertTrue(results.contains(aStockTicker));
        Assertions.assertTrue(results.contains(anotherStockTicker));

        verify(stockTickerService).getByStockSymbol(stockTickerSymbol);
    }

    @Test
    public void testGetByStockSymbolCount() {

        when(stockTickerService.getRowsByStockSymbolCount(stockTickerSymbol)).thenReturn(10);
        int numRows = stockTickerController.getRowsByStockSymbolCount(stockTickerSymbol);

        Assertions.assertEquals(10, numRows);

        verify(stockTickerService).getRowsByStockSymbolCount(stockTickerSymbol);
    }
}
