package com.demo.CodeChallenge.service;

import java.util.LinkedList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;

import com.demo.CodeChallenge.model.StockTicker;
import com.demo.CodeChallenge.repository.StockTickerRepository;

/**
 * This class tests the StockTickerService methods using Mockito.
 */
@ExtendWith(MockitoExtension.class)
public class StockTickerServiceTest {

    private long id;
    private List<StockTicker> stockTickers;
    String stockTickerSymbol;

    @Mock
    private StockTicker aStockTicker;

    @Mock
    private StockTicker anotherStockTicker;

    @Mock
    private StockTickerRepository stockTickerRepository;

    @InjectMocks
    private StockTickerService service;

    @Test
    public void testAdd() {
        service.add(aStockTicker);
        verify(stockTickerRepository).save(aStockTicker);
    }

    @Test
    public void testGetAll() {
        stockTickers = new LinkedList();
        stockTickers.add(aStockTicker);
        stockTickers.add(anotherStockTicker);
        when(stockTickerRepository.findAll()).thenReturn(stockTickers);

        List<StockTicker> results = service.getAll();
        Assertions.assertEquals(2, results.size());
        Assertions.assertTrue(results.contains(aStockTicker));
        Assertions.assertTrue(results.contains(anotherStockTicker));
        verify(stockTickerRepository).findAll();
    }

    @Test
    public void testDeleteById() {
        service.delete(id);
        verify(stockTickerRepository).deleteById(id);
    }

    @Test
    public void testGetByStockSymbol() {
        stockTickers = new LinkedList();
        stockTickers.add(aStockTicker);
        stockTickers.add(anotherStockTicker);
        when(stockTickerRepository.queryByStockTicker(stockTickerSymbol)).thenReturn(stockTickers);
        List<StockTicker> results = service.getByStockSymbol(stockTickerSymbol);

        Assertions.assertEquals(2, results.size());
        Assertions.assertTrue(results.contains(aStockTicker));
        Assertions.assertTrue(results.contains(anotherStockTicker));

        verify(stockTickerRepository).queryByStockTicker(stockTickerSymbol);
    }
    @Test
    public void testGetByStockSymbolCount() {

        when(stockTickerRepository.getRowsByStockSymbolCount(stockTickerSymbol)).thenReturn(10);
        int numRows = service.getRowsByStockSymbolCount(stockTickerSymbol);

        Assertions.assertEquals(10, numRows);

        verify(stockTickerRepository).getRowsByStockSymbolCount(stockTickerSymbol);
    }
}
