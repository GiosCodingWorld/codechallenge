package com.demo.CodeChallenge.writer;

import java.util.LinkedList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.demo.CodeChallenge.model.StockTicker;
import com.demo.CodeChallenge.repository.StockTickerRepository;
import com.demo.CodeChallenge.service.StockTickerService;
import com.demo.CodeChallenge.writer.StockTickerWriter;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * This class tests the StockTickerWriter methods using Mockito.
 */
@ExtendWith(MockitoExtension.class)
public class StockTickerWriterTest {

    private List<StockTicker> stockTickers;

    @Mock
    private StockTicker aStockTicker;

    @Mock
    private StockTicker anotherStockTicker;

    @Mock
    private StockTickerRepository stockTickerRepository;

    @InjectMocks
    private StockTickerWriter stockTickerWriter;

    @Test
    public void testWrite() throws Exception {
        stockTickers = new LinkedList();
        stockTickers.add(aStockTicker);
        stockTickers.add(anotherStockTicker);

        stockTickerWriter.write(stockTickers);

        verify(stockTickerRepository).saveAll(stockTickers);
    }

}
