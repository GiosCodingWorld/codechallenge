package com.demo.CodeChallenge.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.demo.CodeChallenge.model.StockTicker;

/**
 * Interface is used for custom queries.
 * queryByStockTicker: Query by stock ticker.
 * getRowsByStockSymbolCount: For personal validation purposes,
 * a method to return the total number of rows per stock symbol.
 */
public interface StockTickerRepository extends JpaRepository<StockTicker, Long> {

    // Queries by a particular stock ticker symbol.
    @Query(value = "SELECT s FROM StockTicker s WHERE s.stockSymbol =?1")
    List<StockTicker> queryByStockTicker(String stockTickerName);

    // Gets the number of rows returned for a particular stock symbol.
    @Query(value = "SELECT count(*) FROM StockTicker s WHERE s.stockSymbol =?1")
    int getRowsByStockSymbolCount(String stockTickerName);
}
