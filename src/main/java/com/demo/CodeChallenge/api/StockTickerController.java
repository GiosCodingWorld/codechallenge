package com.demo.CodeChallenge.api;

import java.util.List;

import lombok.AllArgsConstructor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.demo.CodeChallenge.model.StockTicker;
import com.demo.CodeChallenge.service.StockTickerService;

/**
 * This class exposes rest endpoints on top of the stock ticker in order to
 * display or perform actions based on the requirements to:
 * - upload a bulk data set;
 * - query for data by a stock ticker
 * - add a new record
 * deleteById and getRowsByStockSymbolCount were used to test only.
 *
 * @author Giovanna Fiorilli
 *
 */
@RestController
@AllArgsConstructor
public class StockTickerController {

    @Autowired
    private final StockTickerService stockTickerService;

    /**
     * Adds a new record.
     * @param stockTicker to be added.
     */
    @PostMapping("/stockTicker")
    public void addStockTicker (@RequestBody StockTicker stockTicker) {
        stockTickerService.add(stockTicker);
    }

    /**
     * Fetches all records.
     * @return all records. Initially the records from the bulk upload.
     */
    @GetMapping("/all")
    public List<StockTicker> all() {
        return stockTickerService.getAll();
    }

    /**
     * Deletes a stock ticker by symbol. This was not per requirement but
     * used to test. To show at demo but be removed if this was a real
     * delivery to a client.
     * @param id the generated record id.
     */
    @DeleteMapping("/deleteById")
    public void deleteById (@RequestParam Long id) {
        stockTickerService.delete(id);
    }

    /**
     * Gets a list of stock tickers by stock symbol.
     * @param stockSymbol the stock ticker used to query.
     * @return all records pertaining to a specific stock symbol/ticker.
     */
    @GetMapping("/getByStockSymbol")
    public List<StockTicker> getByStockSymbol(@RequestParam String stockSymbol) {
        return stockTickerService.getByStockSymbol(stockSymbol);
    }

    /**
     * This method will return the number of rows based on the stock ticker.
     * It is for testing purposes only and not part of the requirement.
     * @param stockSymbol the stock ticker used to query.
     * @return a list of records of the queried dataset by the stockSymbol.
     */
    @GetMapping("/getRowsByStockSymbolCount")
    public int getRowsByStockSymbolCount(@RequestParam String stockSymbol) {
        return stockTickerService.getRowsByStockSymbolCount(stockSymbol);
    }
}
