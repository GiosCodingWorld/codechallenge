package com.demo.CodeChallenge;

import java.util.HashMap;
import java.util.Map;

import java.beans.PropertyEditor;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;

import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.annotation.*;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import com.demo.CodeChallenge.model.StockTicker;
import com.demo.CodeChallenge.writer.StockTickerWriter;

/**
 *  The goal of the CodeChallenge is to achieve the following:
 *  - upload a bulk data set;
 *  - query for data by a stock ticker
 *  - add a new record
 *  - deleteById and getRowsByStockSymbolCount were used to test only.
 *
 * In this class:
 * A batch job is kicked off to upload a dataset from a flat file using Spring
 * batch.
 * Swagger UI is used to test the results and different actions defined by the
 * requirements in this challenge.
 *
 * @author Giovanna Fiorilli
 */
@SpringBootApplication
@EnableSwagger2
@Configuration
@EnableTransactionManagement
@EnableBatchProcessing
public class CodeChallengeApplication {

	public static void main(String[] args) {
		SpringApplication.run(CodeChallengeApplication.class, args);
	}

	@Autowired
	public JobBuilderFactory jobBuilderFactory;

	@Autowired
	public StepBuilderFactory stepBuilderFactory;

	/**
	 * This method is needed for Swagger, which is the UI used
	 * to test our operations.
	 * @return a Docket which is a builder which is intended
	 *         to be the primary interface into the
	 *         swagger-springmvc framework.
	 */
	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.any())
				.paths(PathSelectors.any())
				.build();
	}

	/***
	 * Spring batch will be used to upload the data from the dataset file.
	 * The dataset file is a .csv flat file that is read, uses the column
	 * fields as tokenizers.
	 * It also handles the date format that is read in.
	 *
	 * @return reader
	 */
	@SuppressWarnings({"checkstyle:LineLength", "CheckStyle"})
	@Bean
	@StepScope
	public FlatFileItemReader<StockTicker> stockTickerReader() {
		FlatFileItemReader<StockTicker> reader = new FlatFileItemReader<StockTicker>();
		reader.setResource(new ClassPathResource("data.csv"));
		reader.setLinesToSkip(1);
		reader.setLineMapper(new DefaultLineMapper<StockTicker>() {
			{
				setLineTokenizer(new DelimitedLineTokenizer() {
					{
						setNames(new String[] {
							"yearlyQuarter",
							"stockSymbol",
							"lastBusinessWorkDay",
							"openStockPrice",
							"highestStockPrice",
							"lowestStockPrice",
							"closeStockPrice",
							"stockSharesVolume",
							"percentChangePrice",
							"percentChangeVolumeLastWeek",
							"previousWeekVolume",
							"nextWeekOpeningPrice",
							"nextWeekClosingPrice",
							"nextWeekPercentChangePrice",
							"daysToNextDividend",
							"percentReturnNextDividend"});
					}
				});
				setFieldSetMapper(new BeanWrapperFieldSetMapper<StockTicker>() {
					{
						setTargetType(StockTicker.class);
						// used for the date format
						setCustomEditors(editors());
					}
				});
			}
		});
		return reader;
	}

	/**
	 * This method is used to edit the format of a specific field value,
	 * (in this case it would be the Date that is read in from the .csv file).
	 * It uses the dateEditor() method which defines the date format.
	 * @return editors the type of edits needed on the field.
	 */
	@Bean
	public Map<Class<?>, PropertyEditor> editors() {
		Map<Class<?>, PropertyEditor> editors = new HashMap<>();
		editors.put(LocalDate.class, dateEditor());
		return editors;
	}

	/***
	 * This method will format the date read in so it can be loaded in the
	 * new format.
	 * @return a custom date editor
	 */
	@Bean
	public CustomDateEditor dateEditor() {
		return new CustomDateEditor(new SimpleDateFormat("m/d/yyyy"), false);
	}

	/***
	 * Part of Spring batch framework functionality used to save data.
	 *
	 * @return a stockTicker writer
	 */
	@Bean
	public ItemWriter<StockTicker>  stockTickerWriter() {
		return new StockTickerWriter();
	}

	/***
	 * Part of Spring batch framework functionality.
	 * This defines the batch job and what it does.
	 * step1() represents in this case a batch job that does both the reading
	 * and writing in one (typically there are three steps but this example is
	 * simple and there is no processing step, thereby reading and writing is
	 * combined into a step as is cleaning up).
	 *
	 * @return a stepBuilderFactory which executes the batch job tasks.
	 */
	@Bean
	public Step step1() throws IOException {
		return stepBuilderFactory
				.get("step1")
				.<StockTicker, StockTicker> chunk(100)
				.reader(stockTickerReader())
				.writer(stockTickerWriter())
				.build();
	}

	/***
	 * Part of Spring batch framework functionality.
	 * Creates a job and kickstarts the job.
	 * @return
	 * @throws IOException
	 */
	@Bean
	public Job stockTickerJob() throws IOException {
		return this.jobBuilderFactory.get("stockTickerJob").start(step1()).build();
	}
}

