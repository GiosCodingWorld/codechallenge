package com.demo.CodeChallenge.model;

import java.util.Date;

import javax.persistence.*;

import lombok.*;

/**
 * This class represents the daily statistics of a stock ticker based
 * on a new dataset from the Dow Jones Index from 2011.
 * See the document below for a further description/definition of the fields.
 * @see <a href="http://archive.ics.uci.edu/ml/datasets/Dow+Jones+Index#">Dow Jones Index Data Set</a>
 * @author Giovanna Fiorilli
 *
 */

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StockTicker {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    private Integer yearlyQuarter;
    private String stockSymbol;
    private Date lastBusinessWorkDay;
    private Double openStockPrice;
    private Double highestStockPrice;
    private Double lowestStockPrice;
    private Double closeStockPrice;
    private Long stockSharesVolume;
    private Double percentChangePrice;
    private Double percentChangeVolumeLastWeek;
    private Long previousWeekVolume;
    private Double nextWeekOpeningPrice;
    private Double nextWeekClosingPrice;
    private Double nextWeekPercentChangePrice;
    private Integer daysToNextDividend;
    private Double percentReturnNextDividend;
}
