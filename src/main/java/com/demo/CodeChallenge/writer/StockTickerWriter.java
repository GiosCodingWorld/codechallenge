package com.demo.CodeChallenge.writer;

import java.util.List;

import lombok.extern.slf4j.Slf4j;

import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import com.demo.CodeChallenge.model.StockTicker;
import com.demo.CodeChallenge.repository.StockTickerRepository;


/**
 * This class takes a list of stock tickers and persists them by using the
 * Spring Batch framework.
 * @author Giovanna Fiorilli
 *
 */

@Slf4j
public class StockTickerWriter implements ItemWriter<StockTicker> {

    @Autowired
    private StockTickerRepository stockTickerRepository;

    /**
     * Persists a list of stocktickers.
     * @param stockTickers a list of stock tickers.
     * @throws Exception
     */
    @Override
    public void write(List<? extends StockTicker> stockTickers) throws Exception {
        log.info("persisting {} stock tickers", stockTickers.size());

        stockTickerRepository.saveAll(stockTickers);
    }

}
