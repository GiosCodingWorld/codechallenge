package com.demo.CodeChallenge.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.CodeChallenge.model.StockTicker;
import com.demo.CodeChallenge.repository.StockTickerRepository;

/**
 * This class handles the business requirements logic related to stock tickers.
 * - upload a bulk data set;
 * - query for data by a stock ticker
 * - add a new record
 * - deleteById and getRowsByStockSymbolCount were used to test only.
 *
 * @author Giovanna Fiorilli
 */
@Service
public class StockTickerService {

    @Autowired
    private StockTickerRepository stockTickerRepository;

    /**
     * Adds a new record.
     * @param stockTicker to be added.
     */
    public void add(StockTicker stockTicker) {
        stockTickerRepository.save(stockTicker);
    }

    /**
     * Fetches all records.
     * @return a list of all records (data set).
     */
    public List<StockTicker> getAll() { return stockTickerRepository.findAll(); }

    /**
     * Deletes a stock ticker by symbol. This was not per requirement but
     * used to test. To show at demo but be removed if this was a real
     * delivery to a client.
     * @param id the generated record id.
     */
    public void delete(long id) {
        stockTickerRepository.deleteById(id);
    }

    /**
     * Gets a list of stock tickers by stock symbol.
     * It is for testing purposes only and not part of the requirement.
     * @param stockSymbol the stock ticker used to query.
     * @return a list of records of the queried dataset by the stockSymbol.
     */
    public List<StockTicker> getByStockSymbol(String stockSymbol) {
        return stockTickerRepository.queryByStockTicker(stockSymbol);
    }

    /**
     * This method will return the number of rows based on the stock ticker.
     * It is for testing purposes only and not part of the requirement.
     * @param stockSymbol the stock ticker used to query.
     * @return a list of records of the queried dataset by the stockSymbol.
     */
    public int getRowsByStockSymbolCount(String stockSymbol) {
        return stockTickerRepository.getRowsByStockSymbolCount(stockSymbol);
    }
}
