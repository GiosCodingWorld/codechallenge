package com.demo.CodeChallenge.api;

import java.util.LinkedList;
import java.util.List;

import com.demo.CodeChallenge.model.StockTicker;
import com.demo.CodeChallenge.repository.StockTickerRepository;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;

import static org.assertj.core.api.Assertions.assertThat;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/***
 * Integration tests of the requirements, (add a stock ticker and
 * get stock tickers by symbol.
 * Additional tests can be done with more time.
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class StockTickerControllerIntegrationTest {

    private static final String STOCK_SYMBOL_ONE = "STOCKSYMBOLONE";
    private static final String STOCK_SYMBOL_TWO = "STOCKSYMBOLTWO";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private StockTickerRepository stockTickerRepository;

    private StockTicker stockTickerOne;
    private StockTicker stockTickerTwo;
    private StockTicker stockTickerThree;

    @Test
    void testStockTickerCreation() throws Exception {
        StockTicker stockTicker = new StockTicker();
        stockTicker.setStockSymbol(STOCK_SYMBOL_ONE);

        mockMvc.perform(post("/stockTicker")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(stockTicker)))
                .andExpect(status().isOk());

        mockMvc.perform(post("/stockTicker")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(stockTicker)))
                .andExpect(status().isOk());

        List<StockTicker> stockTickerList = stockTickerRepository.queryByStockTicker(STOCK_SYMBOL_ONE);
        assertThat(stockTickerList.size()).isEqualTo(2);
        assertThat(stockTickerList.get(0).getStockSymbol()).isEqualTo(STOCK_SYMBOL_ONE);
        assertThat(stockTickerList.get(1).getStockSymbol()).isEqualTo(STOCK_SYMBOL_ONE);
    }

    @Test
    void testGetStockTickerBySymbol() throws Exception {
        List<StockTicker> stockTickers = new LinkedList();
        stockTickerOne = new StockTicker();
        stockTickerTwo = new StockTicker();
        stockTickerThree = new StockTicker();

        stockTickerOne.setStockSymbol(STOCK_SYMBOL_ONE);
        stockTickerTwo.setStockSymbol(STOCK_SYMBOL_TWO);
        stockTickerThree.setStockSymbol(STOCK_SYMBOL_ONE);

        stockTickers.add(stockTickerOne);
        stockTickers.add(stockTickerTwo);
        stockTickers.add(stockTickerThree);

        stockTickerRepository.saveAll(stockTickers);

        ResultActions resultActions = mockMvc.perform(get("/getByStockSymbol")
                .queryParam("stockSymbol", STOCK_SYMBOL_ONE)
                .contentType("application/json"))
                .andExpect(status().isOk());

        MvcResult result = resultActions.andReturn();
        String contentAsString = result.getResponse().getContentAsString();

        List<StockTicker> stockTickerList = objectMapper.readValue(contentAsString
                , objectMapper.getTypeFactory().constructCollectionType(List.class, StockTicker.class));

        assertThat(stockTickerList.size()).isEqualTo(2);
        assertThat(stockTickerList.get(0).getStockSymbol()).isEqualTo(STOCK_SYMBOL_ONE);
        assertThat(stockTickerList.get(1).getStockSymbol()).isEqualTo(STOCK_SYMBOL_ONE);

        ResultActions resultActionsTwo = mockMvc.perform(get("/getByStockSymbol")
                .queryParam("stockSymbol", STOCK_SYMBOL_TWO)
                .contentType("application/json"))
                .andExpect(status().isOk());

        MvcResult resultTwo = resultActionsTwo.andReturn();
        String contentAsStringTwo = resultTwo.getResponse().getContentAsString();

        List<StockTicker> stockTickerListTwo = objectMapper.readValue(contentAsStringTwo
                , objectMapper.getTypeFactory().constructCollectionType(List.class, StockTicker.class));

        assertThat(stockTickerListTwo.size()).isEqualTo(1);
        assertThat(stockTickerListTwo.get(0).getStockSymbol()).isEqualTo(STOCK_SYMBOL_TWO);
    }

}
