CodeChallenge was a coding challenge proposed by RBC.

## The requirements are as follows
- upload a data set from a collection of records from the Dow Jones Index containing stock tickers.
- add a new record.
- query by data by stock ticker.
- added a deleteById() method and a getRowsByStockSymbolCount(..) method for self testing and demo show purposes and
  can be later removed.

## Code can be found in the following location in Bitbucket
https://GiosCodingWorld@bitbucket.org/GiosCodingWorld/codechallenge.git

## The technologies used
- Java jdk 11
- Spring Boot 2.2.11
- Maven Project
- JUnit 5

## Dependencies
- Spring Web
- H2 Database
- JDBC API
- JPA
- Springfox (for SWAGGER) as this uses Swagger
- Spring Batch to upload the data
- Logback was added for debugging

## How it was developed and evolved
- the stock ticker model was created first
- add a new record, query by stock ticker, delete by id, count rows by symbol methods created(repository, service, api)
- respective JUnit tests added
- Spring batch functionality added - a reader and writer were created to upload the data as a batch job and JUnit test.
- data.csv was added to the resources in main and test.csv in the test. (This can be improved as well as "magic numbers"
  in future revisions.

## Tests
- JUnit tests for all methods
- Some sample integration tests were added :
  ../src/integrationTest/java/com.demo.CodeChallenge.api/StockTickerControllerIntegrationTest
  These are a few samples for testing, additional tests can be added (and should be).

